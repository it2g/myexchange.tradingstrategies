﻿using Volo.Abp;

namespace MyExchange.TradingStrategies.EntityFrameworkCore
{
    public abstract class TradingStrategiesEntityFrameworkCoreTestBase : TradingStrategiesTestBase<TradingStrategiesEntityFrameworkCoreTestModule> 
    {

    }
}
