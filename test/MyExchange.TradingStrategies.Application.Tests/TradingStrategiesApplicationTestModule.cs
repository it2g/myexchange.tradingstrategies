﻿using Volo.Abp.Modularity;

namespace MyExchange.TradingStrategies
{
    [DependsOn(
        typeof(TradingStrategiesApplicationModule),
        typeof(TradingStrategiesDomainTestModule)
        )]
    public class TradingStrategiesApplicationTestModule : AbpModule
    {

    }
}