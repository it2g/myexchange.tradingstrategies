﻿using MyExchange.TradingStrategies.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace MyExchange.TradingStrategies
{
    [DependsOn(
        typeof(TradingStrategiesEntityFrameworkCoreTestModule)
        )]
    public class TradingStrategiesDomainTestModule : AbpModule
    {

    }
}