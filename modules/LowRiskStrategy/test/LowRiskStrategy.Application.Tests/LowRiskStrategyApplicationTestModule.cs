﻿using Volo.Abp.Modularity;

namespace LowRiskStrategy
{
    [DependsOn(
        typeof(LowRiskStrategyApplicationModule),
        typeof(LowRiskStrategyDomainTestModule)
        )]
    public class LowRiskStrategyApplicationTestModule : AbpModule
    {

    }
}
