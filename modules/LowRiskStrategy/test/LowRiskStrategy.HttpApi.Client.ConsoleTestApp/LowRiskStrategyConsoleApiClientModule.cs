﻿using Volo.Abp.Http.Client.IdentityModel;
using Volo.Abp.Modularity;

namespace LowRiskStrategy
{
    [DependsOn(
        typeof(LowRiskStrategyHttpApiClientModule),
        typeof(AbpHttpClientIdentityModelModule)
        )]
    public class LowRiskStrategyConsoleApiClientModule : AbpModule
    {
        
    }
}
