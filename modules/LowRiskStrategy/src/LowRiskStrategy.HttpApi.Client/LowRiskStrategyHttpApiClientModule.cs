﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Http.Client;
using Volo.Abp.Modularity;

namespace LowRiskStrategy
{
    [DependsOn(
        typeof(LowRiskStrategyApplicationContractsModule),
        typeof(AbpHttpClientModule))]
    public class LowRiskStrategyHttpApiClientModule : AbpModule
    {
        public const string RemoteServiceName = "LowRiskStrategy";

        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddHttpClientProxies(
                typeof(LowRiskStrategyApplicationContractsModule).Assembly,
                RemoteServiceName
            );
        }
    }
}
