﻿using Volo.Abp.Localization;

namespace LowRiskStrategy.Localization
{
    [LocalizationResourceName("LowRiskStrategy")]
    public class LowRiskStrategyResource
    {
        
    }
}
