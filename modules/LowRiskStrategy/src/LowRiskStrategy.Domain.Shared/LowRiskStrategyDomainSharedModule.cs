﻿using Volo.Abp.Modularity;
using Volo.Abp.Localization;
using LowRiskStrategy.Localization;
using Volo.Abp.Localization.ExceptionHandling;
using Volo.Abp.Validation;
using Volo.Abp.Validation.Localization;
using Volo.Abp.VirtualFileSystem;

namespace LowRiskStrategy
{
    [DependsOn(
        typeof(AbpValidationModule)
    )]
    public class LowRiskStrategyDomainSharedModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpVirtualFileSystemOptions>(options =>
            {
                options.FileSets.AddEmbedded<LowRiskStrategyDomainSharedModule>();
            });

            Configure<AbpLocalizationOptions>(options =>
            {
                options.Resources
                    .Add<LowRiskStrategyResource>("en")
                    .AddBaseTypes(typeof(AbpValidationResource))
                    .AddVirtualJson("/Localization/LowRiskStrategy");
            });

            Configure<AbpExceptionLocalizationOptions>(options =>
            {
                options.MapCodeNamespace("LowRiskStrategy", typeof(LowRiskStrategyResource));
            });
        }
    }
}
