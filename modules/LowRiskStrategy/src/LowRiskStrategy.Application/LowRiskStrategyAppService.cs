﻿using LowRiskStrategy.Localization;
using Volo.Abp.Application.Services;

namespace LowRiskStrategy
{
    public abstract class LowRiskStrategyAppService : ApplicationService
    {
        protected LowRiskStrategyAppService()
        {
            LocalizationResource = typeof(LowRiskStrategyResource);
            ObjectMapperContext = typeof(LowRiskStrategyApplicationModule);
        }
    }
}
