﻿using AutoMapper;

namespace LowRiskStrategy
{
    public class LowRiskStrategyApplicationAutoMapperProfile : Profile
    {
        public LowRiskStrategyApplicationAutoMapperProfile()
        {
            /* You can configure your AutoMapper mapping configuration here.
             * Alternatively, you can split your mapping configurations
             * into multiple profile classes for a better organization. */
        }
    }
}