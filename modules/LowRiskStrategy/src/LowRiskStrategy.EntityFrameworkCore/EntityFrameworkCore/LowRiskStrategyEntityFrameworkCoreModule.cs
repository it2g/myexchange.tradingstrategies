﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace LowRiskStrategy.EntityFrameworkCore
{
    [DependsOn(
        typeof(LowRiskStrategyDomainModule),
        typeof(AbpEntityFrameworkCoreModule)
    )]
    public class LowRiskStrategyEntityFrameworkCoreModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddAbpDbContext<LowRiskStrategyDbContext>(options =>
            {
                /* Add custom repositories here. Example:
                 * options.AddRepository<Question, EfCoreQuestionRepository>();
                 */
            });
        }
    }
}