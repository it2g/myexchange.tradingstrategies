﻿using Microsoft.EntityFrameworkCore;
using Volo.Abp.Data;
using Volo.Abp.EntityFrameworkCore;

namespace LowRiskStrategy.EntityFrameworkCore
{
    [ConnectionStringName(LowRiskStrategyDbProperties.ConnectionStringName)]
    public class LowRiskStrategyDbContext : AbpDbContext<LowRiskStrategyDbContext>, ILowRiskStrategyDbContext
    {
        /* Add DbSet for each Aggregate Root here. Example:
         * public DbSet<Question> Questions { get; set; }
         */

        public LowRiskStrategyDbContext(DbContextOptions<LowRiskStrategyDbContext> options) 
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ConfigureLowRiskStrategy();
        }
    }
}