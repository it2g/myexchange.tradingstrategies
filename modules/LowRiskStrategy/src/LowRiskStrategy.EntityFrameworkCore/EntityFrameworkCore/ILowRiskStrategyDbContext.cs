﻿using Volo.Abp.Data;
using Volo.Abp.EntityFrameworkCore;

namespace LowRiskStrategy.EntityFrameworkCore
{
    [ConnectionStringName(LowRiskStrategyDbProperties.ConnectionStringName)]
    public interface ILowRiskStrategyDbContext : IEfCoreDbContext
    {
        /* Add DbSet for each Aggregate Root here. Example:
         * DbSet<Question> Questions { get; }
         */
    }
}