﻿using JetBrains.Annotations;
using Volo.Abp.EntityFrameworkCore.Modeling;

namespace LowRiskStrategy.EntityFrameworkCore
{
    public class LowRiskStrategyModelBuilderConfigurationOptions : AbpModelBuilderConfigurationOptions
    {
        public LowRiskStrategyModelBuilderConfigurationOptions(
            [NotNull] string tablePrefix = "",
            [CanBeNull] string schema = null)
            : base(
                tablePrefix,
                schema)
        {

        }
    }
}