﻿using Volo.Abp.Application;
using Volo.Abp.Modularity;
using Volo.Abp.Authorization;

namespace LowRiskStrategy
{
    [DependsOn(
        typeof(LowRiskStrategyDomainSharedModule),
        typeof(AbpDddApplicationContractsModule),
        typeof(AbpAuthorizationModule)
        )]
    public class LowRiskStrategyApplicationContractsModule : AbpModule
    {

    }
}
