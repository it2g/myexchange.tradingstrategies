﻿using LowRiskStrategy.Localization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;

namespace LowRiskStrategy.Permissions
{
    public class LowRiskStrategyPermissionDefinitionProvider : PermissionDefinitionProvider
    {
        public override void Define(IPermissionDefinitionContext context)
        {
            var myGroup = context.AddGroup(LowRiskStrategyPermissions.GroupName, L("Permission:LowRiskStrategy"));
        }

        private static LocalizableString L(string name)
        {
            return LocalizableString.Create<LowRiskStrategyResource>(name);
        }
    }
}