﻿using Volo.Abp.Reflection;

namespace LowRiskStrategy.Permissions
{
    public class LowRiskStrategyPermissions
    {
        public const string GroupName = "LowRiskStrategy";

        public static string[] GetAll()
        {
            return ReflectionHelper.GetPublicConstantsRecursively(typeof(LowRiskStrategyPermissions));
        }
    }
}