﻿using Localization.Resources.AbpUi;
using LowRiskStrategy.Localization;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Localization;
using Volo.Abp.Modularity;
using Microsoft.Extensions.DependencyInjection;

namespace LowRiskStrategy
{
    [DependsOn(
        typeof(LowRiskStrategyApplicationContractsModule),
        typeof(AbpAspNetCoreMvcModule))]
    public class LowRiskStrategyHttpApiModule : AbpModule
    {
        public override void PreConfigureServices(ServiceConfigurationContext context)
        {
            PreConfigure<IMvcBuilder>(mvcBuilder =>
            {
                mvcBuilder.AddApplicationPartIfNotExists(typeof(LowRiskStrategyHttpApiModule).Assembly);
            });
        }

        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpLocalizationOptions>(options =>
            {
                options.Resources
                    .Get<LowRiskStrategyResource>()
                    .AddBaseTypes(typeof(AbpUiResource));
            });
        }
    }
}
