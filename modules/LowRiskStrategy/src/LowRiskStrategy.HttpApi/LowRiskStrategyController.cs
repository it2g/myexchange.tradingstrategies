﻿using LowRiskStrategy.Localization;
using Volo.Abp.AspNetCore.Mvc;

namespace LowRiskStrategy
{
    public abstract class LowRiskStrategyController : AbpController
    {
        protected LowRiskStrategyController()
        {
            LocalizationResource = typeof(LowRiskStrategyResource);
        }
    }
}
