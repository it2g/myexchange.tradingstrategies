﻿using Volo.Abp.Domain;
using Volo.Abp.Modularity;

namespace LowRiskStrategy
{
    [DependsOn(
        typeof(AbpDddDomainModule),
        typeof(LowRiskStrategyDomainSharedModule)
    )]
    public class LowRiskStrategyDomainModule : AbpModule
    {

    }
}
