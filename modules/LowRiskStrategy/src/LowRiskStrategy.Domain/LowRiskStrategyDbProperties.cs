﻿namespace LowRiskStrategy
{
    public static class LowRiskStrategyDbProperties
    {
        public static string DbTablePrefix { get; set; } = "LowRiskStrategy";

        public static string DbSchema { get; set; } = null;

        public const string ConnectionStringName = "LowRiskStrategy";
    }
}
