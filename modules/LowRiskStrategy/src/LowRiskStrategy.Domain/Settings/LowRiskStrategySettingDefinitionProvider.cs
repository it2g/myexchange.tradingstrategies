﻿using Volo.Abp.Settings;

namespace LowRiskStrategy.Settings
{
    public class LowRiskStrategySettingDefinitionProvider : SettingDefinitionProvider
    {
        public override void Define(ISettingDefinitionContext context)
        {
            /* Define module settings here.
             * Use names from LowRiskStrategySettings class.
             */
        }
    }
}