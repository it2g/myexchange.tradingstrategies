﻿using MyExchange.TradingStrategies.EntityFrameworkCore;
using Volo.Abp.Autofac;
using Volo.Abp.BackgroundJobs;
using Volo.Abp.Modularity;

namespace MyExchange.TradingStrategies.DbMigrator
{
    [DependsOn(
        typeof(AbpAutofacModule),
        typeof(TradingStrategiesEntityFrameworkCoreDbMigrationsModule),
        typeof(TradingStrategiesApplicationContractsModule)
        )]
    public class TradingStrategiesDbMigratorModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpBackgroundJobOptions>(options => options.IsJobExecutionEnabled = false);
        }
    }
}
