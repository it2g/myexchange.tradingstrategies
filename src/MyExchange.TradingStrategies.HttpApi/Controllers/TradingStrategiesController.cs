﻿using MyExchange.TradingStrategies.Localization;
using Volo.Abp.AspNetCore.Mvc;

namespace MyExchange.TradingStrategies.Controllers
{
    /* Inherit your controllers from this class.
     */
    public abstract class TradingStrategiesController : AbpController
    {
        protected TradingStrategiesController()
        {
            LocalizationResource = typeof(TradingStrategiesResource);
        }
    }
}