﻿using MyExchange.TradingStrategies.Localization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;

namespace MyExchange.TradingStrategies.Permissions
{
    public class TradingStrategiesPermissionDefinitionProvider : PermissionDefinitionProvider
    {
        public override void Define(IPermissionDefinitionContext context)
        {
            var myGroup = context.AddGroup(TradingStrategiesPermissions.GroupName);

            //Define your own permissions here. Example:
            //myGroup.AddPermission(TradingStrategiesPermissions.MyPermission1, L("Permission:MyPermission1"));
        }

        private static LocalizableString L(string name)
        {
            return LocalizableString.Create<TradingStrategiesResource>(name);
        }
    }
}
