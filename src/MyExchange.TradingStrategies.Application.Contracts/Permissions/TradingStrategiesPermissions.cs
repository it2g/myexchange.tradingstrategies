﻿namespace MyExchange.TradingStrategies.Permissions
{
    public static class TradingStrategiesPermissions
    {
        public const string GroupName = "TradingStrategies";

        //Add your own permission names. Example:
        //public const string MyPermission1 = GroupName + ".MyPermission1";
    }
}