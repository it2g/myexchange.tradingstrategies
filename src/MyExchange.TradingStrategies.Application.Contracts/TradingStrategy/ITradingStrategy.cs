﻿using System;
using System.Threading.Tasks;
using MyExchange.TradingStrategies.TradingStrategy.Dto;
using Volo.Abp.Application.Services;

namespace MyExchange.TradingStrategies.TradingStrategy
{
    public interface ITradingStrategy : IApplicationService
    {
        string Name { get; }
        ITradingStrategyContext Context { get; set; }
        Task<ITradingStrategy> InitializeAsync(InitializeStrategyInput strategyInput);
        Task DoWorkAsync(Guid strategyId);
        Task RestoreAsync(RestoreInput input);
        Task ShutdownAsync();
    }


    //TODO Когда поймем как задействовать типизированную стратегию
    //то будем использовать этот класс
    public interface ITradingStrategy<TContext> : ITradingStrategy
        where TContext : ITradingStrategyContext
    {
        // TContext Context { get; set; }
    }
   
    
}
