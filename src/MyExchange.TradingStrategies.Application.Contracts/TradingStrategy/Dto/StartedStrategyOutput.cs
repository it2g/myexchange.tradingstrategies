﻿using System;

namespace MyExchange.TradingStrategies.TradingStrategy.Dto
{
    public class StartedStrategyOutput
    {
        public string TradingStrategyName { get; set; }

        public Guid StrategyContextId { get; set; }

    }
}
