﻿namespace MyExchange.TradingStrategies.TradingStrategy.Dto
{
    public class InitializeStrategyInput
    {
        public string TradingStrategyName { get; set; }
    }
}
