﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MyExchange.TradingStrategies.TradingStrategy;
using MyExchange.TradingStrategies.TradingStrategy.Dto;
using Volo.Abp.Application.Services;

namespace MyExchange.TradingStrategies.StrategyManager
{
    public interface IStrategyManager: IApplicationService
    {
        /// <summary>
        /// Запрос на страт приходит через Rest Api
        /// </summary>
        /// <param name="strategyInput"></param>
        /// <returns></returns>
        Task<StartedStrategyOutput> StartStrategyAsync(InitializeStrategyInput strategyInput);

       /// <summary>
       /// Запрос на оснановку приходит через Kafka
       /// </summary>
       /// <param name="strategyContextId"></param>
       /// <returns></returns>
        Task StopStrategyAsync(Guid strategyContextId);
        
        /// <summary>
        /// Останавливает истанс сервиса для перезапуска, например, или переноса его на др. хост
        /// При этом стратегии не останавливаются. Их должен подхватить этот инстанс после перезапуска,
        /// или другой инстанс, если остановленный не будет запущен по истечении установленного времени ожидания.
        /// </summary>
        Task Shutdown();

        /// <summary>
        /// Изменение количества средств, используемых ботом для торговли активами
        /// Требуется дополнительное обдумывание этого метода как с помощью него влиять на 
        /// сумму, выделенную ранее боту
        /// </summary>
        /// <param name="strategyId"></param>
        /// <param name="value"></param>
        Task ChangeValue(Guid strategyId, decimal value);


    }
}
