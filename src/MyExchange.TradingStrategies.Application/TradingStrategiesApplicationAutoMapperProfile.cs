﻿using AutoMapper;

namespace MyExchange.TradingStrategies
{
    public class TradingStrategiesApplicationAutoMapperProfile : Profile
    {
        public TradingStrategiesApplicationAutoMapperProfile()
        {
            /* You can configure your AutoMapper mapping configuration here.
             * Alternatively, you can split your mapping configurations
             * into multiple profile classes for a better organization. */
        }
    }
}
