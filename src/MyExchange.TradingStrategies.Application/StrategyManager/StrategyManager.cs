﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using MyExchange.TradingStrategies.TradingStrategy;
using MyExchange.TradingStrategies.TradingStrategy.Dto;
using Volo.Abp.Application.Services;

namespace MyExchange.TradingStrategies.StrategyManager
{
    public class StrategyManager : ApplicationService, IStrategyManager
    {
        private readonly ITradingStrategy _tradingStrategy;


        public StrategyManager(ITradingStrategy tradingStrategy)
        {
            _tradingStrategy = tradingStrategy;
        }

        /// <summary>
        /// Запрос на страт приходит через Rest Api
        /// </summary>
        /// <param name="strategyInput"></param>
        /// <returns></returns>
        public async Task<StartedStrategyOutput> StartStrategyAsync(InitializeStrategyInput strategyInput)
        {
            StartedStrategyOutput result = null;

            var strategies = ServiceProvider.GetServices<ITradingStrategy>().ToList();
            
            var strategy = strategies.FirstOrDefault(s => s.Name == strategyInput.TradingStrategyName);
            
            if (strategy != null)
            {
                strategy = await strategy.InitializeAsync(strategyInput);
               
                result = new StartedStrategyOutput
                {
                    StrategyContextId = strategy.Context.Id,
                    TradingStrategyName = strategy.Name
                };
            }

            return result;
        }

        /// <summary>
        /// Запрос на оснановку приходит через Kafka
        /// </summary>
        /// <param name="strategyContextId"></param>
        /// <returns></returns>
        public Task StopStrategyAsync(Guid strategyContextId)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Останавливает истанс сервиса для перезапуска, например, или переноса его на др. хост
        /// При этом стратегии не останавливаются. Их должен подхватить этот инстанс после перезапуска,
        /// или другой инстанс, если остановленный не будет запущен по истечении установленного времени ожидания.
        /// </summary>
        public Task Shutdown()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Изменение количества средств, используемых ботом для торговли активами
        /// Требуется дополнительное обдумывание этого метода как с помощью него влиять на 
        /// сумму, выделенную ранее боту
        /// </summary>
        /// <param name="value"></param>
        public Task ChangeValue(Guid strategyId, decimal value)
        {
            throw new NotImplementedException();
        }

    }
}
