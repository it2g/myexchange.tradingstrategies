﻿using System;
using System.Collections.Generic;
using System.Text;
using MyExchange.TradingStrategies.Localization;
using Volo.Abp.Application.Services;

namespace MyExchange.TradingStrategies
{
    /* Inherit your application services from this class.
     */
    public abstract class TradingStrategiesAppService : ApplicationService
    {
        protected TradingStrategiesAppService()
        {
            LocalizationResource = typeof(TradingStrategiesResource);
        }
    }
}
