﻿using Microsoft.EntityFrameworkCore;
using Volo.Abp;

namespace MyExchange.TradingStrategies.EntityFrameworkCore
{
    public static class TradingStrategiesDbContextModelCreatingExtensions
    {
        public static void ConfigureTradingStrategies(this ModelBuilder builder)
        {
            Check.NotNull(builder, nameof(builder));

            /* Configure your own tables/entities inside here */

            //builder.Entity<YourEntity>(b =>
            //{
            //    b.ToTable(TradingStrategiesConsts.DbTablePrefix + "YourEntities", TradingStrategiesConsts.DbSchema);
            //    b.ConfigureByConvention(); //auto configure for the base class props
            //    //...
            //});
        }
    }
}