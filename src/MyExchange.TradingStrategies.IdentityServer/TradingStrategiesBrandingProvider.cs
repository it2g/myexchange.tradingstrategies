﻿using Volo.Abp.Ui.Branding;
using Volo.Abp.DependencyInjection;

namespace MyExchange.TradingStrategies
{
    [Dependency(ReplaceServices = true)]
    public class TradingStrategiesBrandingProvider : DefaultBrandingProvider
    {
        public override string AppName => "TradingStrategies";
    }
}
