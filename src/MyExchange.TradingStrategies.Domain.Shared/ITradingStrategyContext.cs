﻿using System;
using System.Collections.Generic;
using System.Text;
using MyExchange.TradingStrategies.Constants;
using Volo.Abp;

namespace MyExchange.TradingStrategies
{
    public interface ITradingStrategyContext
    {
        Guid Id { get; }

        /// <summary>
        /// Идентификатор пользователя, запустившего торговлю
        /// </summary>
        long UserId { get; set; }

        /// <summary>
        /// Спепень риска указанная пользователем
        /// </summary>
        RiskDegrees RiskDegree { get; set; }

        /// <summary>
        /// Продолжительность торговой сессии заказанная пользователем
        /// </summary>
        TimeSpan Duration { get; set; }
    }
}
