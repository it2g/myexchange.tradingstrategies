﻿using Volo.Abp.Localization;

namespace MyExchange.TradingStrategies.Localization
{
    [LocalizationResourceName("TradingStrategies")]
    public class TradingStrategiesResource
    {

    }
}