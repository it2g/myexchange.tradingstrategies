﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyExchange.TradingStrategies.Constants
{
    public enum RiskDegrees
    {
        LowRisk = 0,

        MiddleRisk = 1,

        HighRisk = 2
    }
}
