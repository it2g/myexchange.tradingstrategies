﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Modularity;

namespace MyExchange.TradingStrategies.EntityFrameworkCore
{
    [DependsOn(
        typeof(TradingStrategiesEntityFrameworkCoreModule)
        )]
    public class TradingStrategiesEntityFrameworkCoreDbMigrationsModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddAbpDbContext<TradingStrategiesMigrationsDbContext>();
        }
    }
}
