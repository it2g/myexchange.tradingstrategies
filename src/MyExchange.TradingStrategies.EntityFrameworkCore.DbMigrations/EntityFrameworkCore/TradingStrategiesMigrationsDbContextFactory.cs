﻿using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace MyExchange.TradingStrategies.EntityFrameworkCore
{
    /* This class is needed for EF Core console commands
     * (like Add-Migration and Update-Database commands) */
    public class TradingStrategiesMigrationsDbContextFactory : IDesignTimeDbContextFactory<TradingStrategiesMigrationsDbContext>
    {
        public TradingStrategiesMigrationsDbContext CreateDbContext(string[] args)
        {
            TradingStrategiesEfCoreEntityExtensionMappings.Configure();

            var configuration = BuildConfiguration();

            var builder = new DbContextOptionsBuilder<TradingStrategiesMigrationsDbContext>()
                .UseNpgsql(configuration.GetConnectionString("Default"));

            return new TradingStrategiesMigrationsDbContext(builder.Options);
        }

        private static IConfigurationRoot BuildConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Path.Combine(Directory.GetCurrentDirectory(), "../MyExchange.TradingStrategies.DbMigrator/"))
                .AddJsonFile("appsettings.json", optional: false);

            return builder.Build();
        }
    }
}
