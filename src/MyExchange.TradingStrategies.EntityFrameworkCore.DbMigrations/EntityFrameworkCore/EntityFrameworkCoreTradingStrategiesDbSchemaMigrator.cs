﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MyExchange.TradingStrategies.Data;
using Volo.Abp.DependencyInjection;

namespace MyExchange.TradingStrategies.EntityFrameworkCore
{
    public class EntityFrameworkCoreTradingStrategiesDbSchemaMigrator
        : ITradingStrategiesDbSchemaMigrator, ITransientDependency
    {
        private readonly IServiceProvider _serviceProvider;

        public EntityFrameworkCoreTradingStrategiesDbSchemaMigrator(
            IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task MigrateAsync()
        {
            /* We intentionally resolving the TradingStrategiesMigrationsDbContext
             * from IServiceProvider (instead of directly injecting it)
             * to properly get the connection string of the current tenant in the
             * current scope.
             */

            await _serviceProvider
                .GetRequiredService<TradingStrategiesMigrationsDbContext>()
                .Database
                .MigrateAsync();
        }
    }
}