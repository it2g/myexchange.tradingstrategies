﻿using System;
using System.Collections.Generic;
using System.Text;
using MyExchange.TradingStrategies.Constants;
using Volo.Abp.Domain.Entities.Auditing;

namespace MyExchange.TradingStrategies.TradingStrategy
{
    public abstract class TradingStrategyContextBase  : FullAuditedAggregateRoot<Guid>, ITradingStrategyContext
    {
        /// <summary>
        /// Идентификатор пользователя, запустившего торговлю
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// Спепень риска указанная пользователем
        /// </summary>
        public RiskDegrees RiskDegree { get; set; }

        /// <summary>
        /// Продолжительность торговой сессии заказанная пользователем
        /// </summary>
        public TimeSpan Duration { get; set; }
    }
}
