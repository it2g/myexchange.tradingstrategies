﻿using Volo.Abp.Settings;

namespace MyExchange.TradingStrategies.Settings
{
    public class TradingStrategiesSettingDefinitionProvider : SettingDefinitionProvider
    {
        public override void Define(ISettingDefinitionContext context)
        {
            //Define your own settings here. Example:
            //context.Add(new SettingDefinition(TradingStrategiesSettings.MySetting1));
        }
    }
}
