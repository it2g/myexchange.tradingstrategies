﻿using System.Threading.Tasks;

namespace MyExchange.TradingStrategies.Data
{
    public interface ITradingStrategiesDbSchemaMigrator
    {
        Task MigrateAsync();
    }
}
