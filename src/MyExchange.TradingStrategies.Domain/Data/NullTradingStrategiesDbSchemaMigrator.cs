﻿using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;

namespace MyExchange.TradingStrategies.Data
{
    /* This is used if database provider does't define
     * ITradingStrategiesDbSchemaMigrator implementation.
     */
    public class NullTradingStrategiesDbSchemaMigrator : ITradingStrategiesDbSchemaMigrator, ITransientDependency
    {
        public Task MigrateAsync()
        {
            return Task.CompletedTask;
        }
    }
}